//
//  AppDelegate.swift
//  KeystrokeTester
//
//  Created by Chris Parker on 2/21/19.
//  Copyright © 2019 Chris Parker. All rights reserved.
//

import Cocoa

struct KeystrokeInfo: Codable {
    var keyCode: UInt16
    var rawModifiers: UInt
    var chars: String
}

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate {

    @IBOutlet weak var window: NSWindow!
    @IBOutlet weak var viewer: KeystrokeCaptureView!

    func applicationDidFinishLaunching(_ aNotification: Notification) {
        let defs = UserDefaults.standard

        viewer.onCapture = { (anEvent: NSEvent) in
            let keyInfo = KeystrokeInfo(keyCode: anEvent.keyCode, rawModifiers: anEvent.modifierFlags.rawValue, chars: anEvent.charactersIgnoringModifiers ?? "")
            let jsonEncoder = JSONEncoder()
            if let encodedKeyInfo = try? jsonEncoder.encode(keyInfo) {
                defs.set(encodedKeyInfo, forKey: "UserSelectedHotKey")
            }
            defs.synchronize()
        }

        if let encodedKeyInfo = defs.object(forKey: "UserSelectedHotKey") as? Data {
            let decoder = JSONDecoder()
            if let keyInfo = try? decoder.decode(KeystrokeInfo.self, from: encodedKeyInfo) {
                viewer.setDisplayValue(keyCode: keyInfo.keyCode, modifiers: NSEvent.ModifierFlags(rawValue: keyInfo.rawModifiers), chars: keyInfo.chars)
            }
        }
    }

    func applicationWillTerminate(_ aNotification: Notification) {}
}
