//
//  KeystrokeCaptureView.swift
//  KeystrokeTester
//
//  Created by Chris Parker on 2/25/19.
//  Copyright © 2019 Chris Parker. All rights reserved.
//

import Cocoa

class KeystrokeCaptureView: NSView {
    var viewLabel: NSTextField = NSTextField.init(labelWithString: "Key:")
    var keystrokeLabel: NSTextField = NSTextField.init(labelWithString: "None")
    var keyDownMonitor: Any? = nil
    var KeyUpMonitor: Any? = nil
    var onCapture: ((NSEvent) -> ())?

    func _sharedInit() {
        // TODO: This should all be auto-layout-ified.
        viewLabel.frame = NSRect(origin: CGPoint(x: 0.0, y: 7.0), size: viewLabel.fittingSize)
        keystrokeLabel.frame = NSRect(x: viewLabel.frame.width + 5.0, y: 0.0, width: self.bounds.width - viewLabel.frame.width - 5.0, height: self.bounds.height)
        keystrokeLabel.isBezeled = true
        keystrokeLabel.bezelStyle = .roundedBezel
        keystrokeLabel.alignment = .center
        self.addSubview(viewLabel)
        self.addSubview(keystrokeLabel)
    }
    
    override init(frame frameRect: NSRect) {
        super.init(frame: frameRect)
        _sharedInit()
    }
    
    required init?(coder decoder: NSCoder) {
        super.init(coder: decoder)
        _sharedInit()
    }

    override var isFlipped: Bool { return true }
    override var acceptsFirstResponder: Bool { return true }
    override func becomeFirstResponder() -> Bool { return true }
    override func resignFirstResponder() -> Bool { return true }
    
    override func mouseDown(with event: NSEvent) {
        keystrokeLabel.stringValue = "Capturing..."
    }

    override func mouseUp(with event: NSEvent) {
        guard keyDownMonitor == nil else {
            return
        }

        keyDownMonitor = NSEvent.addLocalMonitorForEvents(matching: .keyDown, handler: { (anEvent) -> NSEvent? in
            return nil
        })
        KeyUpMonitor = NSEvent.addLocalMonitorForEvents(matching: .keyUp, handler: { (anEvent) -> NSEvent? in
            NSEvent.removeMonitor(self.keyDownMonitor!)
            NSEvent.removeMonitor(self.KeyUpMonitor!)
            self.keyDownMonitor = nil
            self.KeyUpMonitor = nil
            self.keystrokeLabel.stringValue = self.stringForKeyEvent(anEvent: anEvent)
            self.onCapture?(anEvent)
            return nil
        })
    }

    func setDisplayValue(keyCode: UInt16, modifiers: NSEvent.ModifierFlags, chars: String?) {
        keystrokeLabel.stringValue = stringFor(keyCode: keyCode, modifiers: modifiers, chars: chars)
    }

    func stringFor(keyCode: UInt16, modifiers: NSEvent.ModifierFlags, chars: String?) -> String {
        let f = modifiers
        var s = ""

        if f.contains(.control) {
            s.append("⌃")
        }
        if f.contains(.command) {
            s.append("⌘")
        }
        if f.contains(.option) {
            s.append("⌥")
        }
        if f.contains(.shift) {
            s.append("⇧")
        }

        var key = ""
        switch keyCode {
        case 49:    key = " Space"
        case 76:    key = " Enter"
        case 36:    key = " Return"
        case 116:   key = " PgUp"
        case 121:   key = " PgDn"
        case 115:   key = " Home"
        case 119:   key = " End"
        case 48:    key = " Tab"
        case 122:   key = "F1"
        case 120:   key = "F2"
        case 99:    key = "F3"
        case 118:   key = "F4"
        case 96:    key = "F5"
        case 97:    key = "F6"
        case 98:    key = "F7"
        case 100:   key = "F8"
        case 101:   key = "F9"
        case 109:   key = "F10"
        case 103:   key = "F11"
        case 111:   key = "F12"
        default:
            if chars != nil {
                key = chars!.uppercased()
            }
        }

        s.append(key)

        return s
    }

    func stringForKeyEvent(anEvent: NSEvent) -> String {
        return stringFor(keyCode: anEvent.keyCode, modifiers: anEvent.modifierFlags, chars: anEvent.charactersIgnoringModifiers)
    }
}
